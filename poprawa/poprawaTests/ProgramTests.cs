﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using poprawa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace poprawa.Tests
{
        [TestClass()]
        public class ProgramTests
        {
            [TestMethod()]
            public void WyznaczKatTest()
            {
                var p1 = new Punkt(1, 1);
                var p2 = new Punkt(4, 10);
                var kat = Program.WyznaczKat(p1, p2);

                Assert.AreEqual(0.950415280255183, kat);
            }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace poprawa
{
    public class Program
    {
        static void Main(string[] args)
        {
            var p1 = new Punkt(1, 2);
            var p2 = new Punkt(10, 10);

            var kat = WyznaczKat(p1, p2);
        }

        public static double WyznaczKat(Punkt p1, Punkt p2)
        {
            double DlugoscProstej = Math.Sqrt(Math.Pow(p2.X - p1.X, 2) + Math.Pow(p2.Y - p1.Y, 2));
            double a = Math.Abs(p1.X - p2.X);
            return Math.Cos(a / DlugoscProstej);
        }
    }

    public class Punkt
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Punkt(double x, double y)
        {
            X = x;
            Y = y;
        }
    }
}
